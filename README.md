# Helm Chart with Nginx, Apache2, and Tomcat Web Servers

More details and explanations in the Medium blog: https://link.medium.com/8iBUhRF21xb

## Overview

This project consists of multiple environments, including development, staging, production, etc.

In each environment, the parameters required for deploying the environment may vary. For instance, the number of replicas needed for deployments may differ, as well as the ingress routing rules, configuration and secret parameters, and other environment-specific settings.

By using this Helm chart, we only need to maintain one file (`values.yaml`) that can be customized for each environment through a single values file.

## About the Project

We have 3 applications with web servers: Nginx, Apache2, and Tomcat. Traffic is going to the Ingress, then to the Nginx web server. In this case, Nginx works as a reverse proxy and routes traffic to Apache2 and Tomcat web servers. We have preconfigured default web pages on each web server with information about the environment name and version of the web server. Preconfigured default Nginx web page has 2 buttons which are provided to Apache2 and Tomcat web servers. Ingress is secured with SSL/TLS certificate.

![Connections schema](nginx_apache2_tomcat.png)

We have preconfigured default web pages on each web server with information about the environment name and version of the web server. Preconfigured default Nginx web page has 2 buttons which are provided to Apache2 and Tomcat web servers.

![Main web page](main_page_servers.png)

## Prerequisites

To begin using Helm charts, there are certain requirements that you must have:

1. A working Kubernetes cluster. I used minikube with Nginx Ingress Controller as addon. It is necessary in this case to add `hostname` with minikube IP address into `/etc/hosts` and optionally generate self-signed SSL/TLS certificate with openssl.
2. Helm installed on your workstation.

## Steps to Follow

1. Clone the Git repository and make necessary changes in `values.yaml` file.

2. Verify the Helm chart's configuration:
```
helm lint helm-chart-nginx-httpd-tomcat/
```

3. To ensure that the values are correctly substituted in the templates, use the following command to render the templated YAML files with the values:
```
helm template helm-chart-nginx-httpd-tomcat/
```

4. Use the `--dry-run` flag to simulate the installation process and catch any issues that may arise:
```
helm install web-servers helm-chart-nginx-httpd-tomcat/ --dry-run
```

5. Deploy the Helm Chart to the Kubernetes Cluster:
```
helm install web-servers helm-chart-nginx-httpd-tomcat/
```

6. To uninstall the chart, which will eliminate all resources connected with the last chart release, use the following command:
```
helm uninstall web-servers
```

## Optional Steps

* Check the release list with:
```
helm list
```

* To overwrite the default values file and install a Helm chart with external `values.yaml` file, use the following command with the `--values` flag and path of the values file:
```
helm install web-servers helm-chart-nginx-httpd-tomcat/ --values env/prod-values.yaml
```

* To update the chart and apply the changes, use the following command:
```
helm upgrade web-servers helm-chart-nginx-httpd-tomcat/
```

* To undo the changes made earlier and redeploy the previous version, use the rollback command:
```
helm rollback web-servers
```

* To rollback to a specific version, include the revision number:
```
helm rollback web-servers 3
```

* Command for generation SSL/TLS key with openssl:
```
openssl req -x509 -newkey rsa:4096 -sha256 -days 3650 -nodes \
  -keyout example.key -out example.crt -subj "/CN=simleappp.com" \
  -addext "subjectAltName=DNS:simleappp.com,DNS:www.simleappp.com,IP:192.168.49.2"
```
